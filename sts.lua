-- ISC License
--
-- Copyright (c) 2019, Andrea D'Amore
--
-- Permission to use, copy, modify, and/or distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

module = {}

vis:option_register("strip-trailing-spaces", "bool", function(value, toogle)
    if not vis.win then
        return false
    end
    vis.win.strip_trailing_spaces = toogle and not vis.win.strip_trailing_spaces or value
    -- vis:info("Option strip-trailing-spaces = " .. tostring(vis.win.strip_trailing_spaces))
    return true
end, "Strip line trailing spaces on save")

vis:command_register("sts", function(argv, force, win, selection, range)
    if win.strip_trailing_spaces then
        local lines = win.file.lines
        for index = 1, #lines do
            lines[index] = lines[index]:gsub("%s+$", "")
        end
        vis:info("Line trailing spaces trimmed." .. type(win.file.lines))
    end
    return true;
end, "Strip line trailing spaces"
)

vis.events.subscribe(vis.events.FILE_SAVE_PRE, function(file, path)
    vis:command("sts")
    return nil
end, 1)

-- vis.events.subscribe(vis.events.WIN_OPEN, function(win)
--     vis:command("set strip-trailing-spaces on")
-- end)

return module

