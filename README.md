# vis-strip-trailing-spaces

A [vis](https://martanne.github.io/vis/) plugin that strip trailing spaces on lines.

# Install

Copy the lua package or clone the repository where visrc.lua can access it then load it, cf. [Plugins](https://github.com/martanne/vis/wiki/Plugins).

Example:
`
    mkdir -p ~/.config/lua/plugins
    git clone https://gitlab.com/anddam/vis-strip-trailing-spaces.git ~/.config/lua/plugins


# Usage

The plugin register on a file save pre-hook, you can toggle its behavior on or off with the `strip-trailing-spaces` option, i.e. with `:set strip-trailing-spaces on` or `off`.

The option is of type `bool` so you can toggle its current state by appending `!` to the option name.

In order to load the plugin add

    require("plugins/vis-strip-trailing-spaces/vis-strip-trailing-spaces")

to `~/.config/vis/visrc.lua`, then enable stripping behavior with:

    vis.events.subscribe(vis.events.WIN_OPEN, function(win)
        vis:command("set strip-trailing-spaces on")
    end)
